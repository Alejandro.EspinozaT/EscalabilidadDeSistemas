INSERT INTO sales (sale_date, amount) VALUES ('2024-04-15', 100.00);
INSERT INTO sales (sale_date, amount) VALUES ('2024-05-15', 200.00);

INSERT INTO orders (order_status, order_date) VALUES ('Pending', '2024-05-10');
INSERT INTO orders (order_status, order_date) VALUES ('Completed', '2024-05-11');
INSERT INTO orders (order_status, order_date) VALUES ('Cancelled', '2024-05-12');

INSERT INTO users (username, created_at) VALUES ('user1', '2024-04-01');
INSERT INTO users (username, created_at) VALUES ('user2', '2024-05-02');
